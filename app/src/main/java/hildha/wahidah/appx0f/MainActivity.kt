package hildha.wahidah.appx0f

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var bundle: Bundle? = null
    var topik = "appx0f"
    var type = 0
    var token = "AAAAKqkN0tE:APA91bHvDkwr139V3-NNyPQ1OMPMLL74atU5N6720CvO2ZEKg3AbIBaKD4Z4c6zXgn2xwYtwFsuUJTsJglIY-CqvR4BKl0yaQhSloIez8yXm8024RPgnh39vBByACf59qteWT7MBZASI"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        edToken.setText(token)
        FirebaseMessaging.getInstance().subscribeToTopic(topik)



    }
    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
            OnCompleteListener { task ->
                if (task.isSuccessful) return@OnCompleteListener
                edToken.setText(task.result!!.token)
            }
        )
        try {
            bundle = intent.extras
        } catch (e: Exception) {
            Log.e("BUNDLE", "bundle is null")
        }

        if (bundle !=null){
            type = bundle!!.getInt("type")
            when(type){
                0->{
                    edPromoId.setText(bundle!!.getString("promoId"))
                    edPromo.setText(bundle!!.getString("promo"))
                    edPromoUntil.setText(bundle!!.getString("promoUntil"))
                }
                1->{
                    edTitle.setText(bundle!!.getString("title"))
                    edBody.setText(bundle!!.getString("body"))
                }
            }
        }

    }

}

